﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TrueFakers_ale.Models;

namespace TrueFakers_ale.Data
{
    public class ApplicationDbContext : IdentityDbContext<Usuario>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Comentario> Comentario { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Noticia> Noticia { get; set; }
        public DbSet<Voto> Voto { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityRole>().HasData(new IdentityRole { Name="usuario", NormalizedName="USUARIO"});
            builder.Entity<IdentityRole>().HasData(new IdentityRole { Name="editor", NormalizedName="EDITOR"});
            builder.Entity<IdentityRole>().HasData(new IdentityRole { Name="admin", NormalizedName="ADMIN"});
        }

    }
}
