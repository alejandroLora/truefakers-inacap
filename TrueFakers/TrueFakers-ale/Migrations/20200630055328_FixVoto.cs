﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TrueFakers_ale.Migrations
{
    public partial class FixVoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "6adf5821-f45b-40ec-970c-f519a2a184df");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f2abe8f3-2f70-4a2a-abdb-9d08fc5461ea");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f59738fd-42c6-42ae-b064-e645fc58a232");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "52f07707-519a-4ca7-941b-ff0befd55182", "54129c73-3a73-4f84-85c9-c221b3233668", "usuario", "USUARIO" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "b754a118-d4d0-4741-8d5e-5d920f9ec988", "710414fa-2359-4c67-a059-8f94bf335c1d", "editor", "EDITOR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "30aa9ca9-ae5c-4f40-bd9a-60adfc172ae0", "62c1230c-5103-462d-b854-3f184d333d77", "admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "30aa9ca9-ae5c-4f40-bd9a-60adfc172ae0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "52f07707-519a-4ca7-941b-ff0befd55182");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b754a118-d4d0-4741-8d5e-5d920f9ec988");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "f2abe8f3-2f70-4a2a-abdb-9d08fc5461ea", "16028335-d392-4fa8-9062-23fc9c10886a", "usuario", "USUARIO" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "f59738fd-42c6-42ae-b064-e645fc58a232", "b1ed71e6-1db5-49c6-8be8-fcbada537464", "editor", "EDITOR" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "6adf5821-f45b-40ec-970c-f519a2a184df", "a491b6ce-fc14-46c5-8ffd-14c72af29528", "admin", "ADMIN" });
        }
    }
}
