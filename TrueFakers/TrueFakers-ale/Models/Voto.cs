﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrueFakers_ale.Models
{
    public class Voto
    {
        public int VotoId { get; set; }
        public int Tipo { get; set; } //0: Pendiente, 1: falso, 2: verdadero: 3: impreciso
        public virtual Noticia Noticia { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
