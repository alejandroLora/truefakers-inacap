﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrueFakers_ale.Models
{
    public class Usuario: IdentityUser
    {
        public int Puntaje { get; set; }
        public bool IsBlocked { get; set; } = false;
        public virtual ICollection<Comentario> Comentarios { get; set; }
        public virtual ICollection<Noticia> Noticias { get; set; }
        public virtual ICollection<Voto> Votos { get; set; }
    }
}
