﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrueFakers_ale.Models
{
    public class Noticia
    {
        public int NoticiaId { get; set; }
        public DateTime Fecha { get; set; }
        public int Estado { get; set; }
        public string Titulo { get; set; }
        public string SubTitulo { get; set; }
        public string Cuerpo { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
        public int Veredicto { get; set; } = 0;
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<Comentario> Comentarios { get; set; }
        public virtual ICollection<Voto> Votos { get; set; }
    }
}
