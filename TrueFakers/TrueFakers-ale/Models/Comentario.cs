﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrueFakers_ale.Models
{
    public class Comentario
    {
        public int ComentarioId { get; set; }
        public string Contenido { get; set; }
        public DateTime Fecha { get; set; }
        public int Estado { get; set; }
        public int Puntaje { get; set; }
        public int Reportes { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Noticia Noticia { get; set; }
    }
}
