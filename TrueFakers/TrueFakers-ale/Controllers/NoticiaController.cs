﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrueFakers_ale.Data;
using TrueFakers_ale.Models;

namespace TrueFakers_ale.Controllers
{
    [Authorize]
    public class NoticiaController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<Usuario> userManager;
        private readonly SignInManager<Usuario> signManager;

        public NoticiaController(ApplicationDbContext context, UserManager<Usuario> userManager, SignInManager<Usuario> signManager)
        {
            _context = context;
            this.userManager = userManager;
            this.signManager = signManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            IEnumerable<Noticia> noticias = await GetNoticiasByEnabled(true);
            return View(noticias);
        }

        [HttpGet]
        public async Task<IActionResult> NoAprobadas()
        {
            IEnumerable<Noticia> noticias = await GetNoticiasByEnabled(false);
            return View("Index", noticias);
        }

        [HttpGet]
        public async Task<IActionResult> Falsas()
        {
            List<Noticia> noticias = await GetNoticiasByVeredicto(false);
            return View("Index", noticias);
        }

        [HttpGet]
        public async Task<IActionResult> Verdaderas()
        {
            List<Noticia> noticias = await GetNoticiasByVeredicto(true);
            return View("Index", noticias);
        }

        [HttpGet]
        public async Task<IActionResult> Imprecisas()
        {
            List<Noticia> noticias = await GetNoticiasByVeredicto();
            return View("Index", noticias);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Noticia model)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                Noticia noticia = new Noticia
                {
                    Titulo = model.Titulo,
                    SubTitulo = model.SubTitulo,
                    Cuerpo = model.Cuerpo,
                    Fecha = DateTime.Now,
                    IsEnabled = false,
                    IsVerified = false,
                    Estado = 0,
                    Veredicto = 0,
                    Usuario = await GetUserById(currentUserId)
                };

                _context.Noticia.Add(noticia);
                int result = await _context.SaveChangesAsync();

                if (result != 0)
                {
                    return RedirectToAction("index", "home");
                }
                else
                {
                    return NoContent();
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Noticia noticia = await GetNoticia(id);
            List<Comentario> comentarios = await GetComentarios(noticia);

            if (noticia == null)
            {
                return NotFound();
            }

            ViewData["HavePermission"] = false;

            if (signManager.IsSignedIn(User))
            {
                string currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                if (User.IsInRole("editor") || User.IsInRole("admin") || (currentUserId.Equals(noticia.Usuario.Id)))
                {
                    ViewData["HavePermission"] = true;
                }
            }

            ViewData["CommentsList"] = comentarios;
            return View(noticia);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var noticia = await _context.Noticia.FindAsync(id);
            if (noticia == null)
            {
                return NotFound();
            }
            return View(noticia);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Noticia model)
        {
            if (id == model.NoticiaId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Noticia noticia = await _context.Noticia.FindAsync(id);
                noticia.Titulo = model.Titulo;
                noticia.SubTitulo = model.SubTitulo;
                noticia.Cuerpo = model.Cuerpo;
                noticia.IsEnabled = model.IsEnabled;
                noticia.Veredicto = model.Veredicto;
                noticia.Estado = model.Estado;

                if(model.Veredicto != 0)
                {
                    noticia.IsVerified = true;
                }
                else
                {
                    noticia.IsVerified = false;
                }

                if(!noticia.IsVerified)
                {
                    noticia.Veredicto = 0;
                }

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch
                {
                    throw;
                }
                return RedirectToAction("index", "home");
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Search(string query)
        {
            ViewData["SearchNoticia"] = query;

            var searchQuery = from x in _context.Noticia select x;
            if (!String.IsNullOrEmpty(query))
            {
                if (User.IsInRole("editor") || User.IsInRole("admin") || User.IsInRole("usuario"))
                {
                    searchQuery = searchQuery.Where(x => x.Titulo.Contains(query));
                } 
                else
                {
                    searchQuery = searchQuery.Where(x => x.Titulo.Contains(query) && x.Veredicto == 2 && x.IsVerified == true);
                }
                
            }
            return View(await searchQuery.AsNoTracking().ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> ListVeredictos()
        {
            //IList<string> roleOfRandomUser = new List<string>();

            //while (!roleOfRandomUser.Contains("editor"))
            //{
            //    Random rnd = new Random();

            //    List<Usuario> allUsers = await userManager.Users.ToListAsync();
            //    List<Usuario> shuffleUsers = allUsers.OrderBy(x => rnd.Next()).ToList();

            //    Usuario randomUser = shuffleUsers.Take(1).First();
            //    roleOfRandomUser = await userManager.GetRolesAsync(randomUser);

            //    if (roleOfRandomUser.Contains("editor"))
            //    {
            //        ViewData["randomEditor"] = randomUser.Id;
            //    }
            //}

            List<Noticia> noticias = await _context.Noticia.Where(n => n.Estado == 1).ToListAsync();

            return View(noticias);
        }


        //[HttpPost]
        //public async Task<IActionResult> Veredicto(int noticiaId, Noticia model)
        //{
        //    Noticia copyNoticia = await GetNoticia(noticiaId);

        //    if (ModelState.IsValid)
        //    {
        //        copyNoticia.Veredicto = model.Veredicto;
        //        RedirectToAction("", "");
        //    }

        //    return View(copyNoticia);
        //}


        public async Task<Usuario> GetUserById(string currentUserId)
        {
            Usuario user = await userManager.FindByIdAsync(currentUserId);
            return user;
        }

        public async Task<List<Comentario>> GetComentarios(Noticia id)
        {
            List<Comentario> comentarios = await _context.Comentario.Include(c => c.Usuario).Where(c => c.Noticia == id).ToListAsync();
            return comentarios;
        }

        public async Task<Noticia> GetNoticia(int? id)
        {
            Noticia noticia = await _context.Noticia.Include(n => n.Usuario).Include(n => n.Comentarios).FirstOrDefaultAsync(m => m.NoticiaId == id);
            return noticia;
        }

        public async Task<List<Noticia>> GetNoticiasByVeredicto(bool veredicto)
        {
            return await GetNoticiasByVeredicto(veredicto, false);
        }

        public async Task<IEnumerable<Noticia>> GetNoticiasByEnabled(bool enabledNews)
        {
            // Este metodo retorna una lista de noticias segun si estan aceptadas o no para ser verificadas
            IEnumerable<Noticia> _noticias = await _context.Noticia.Where(n => (n.IsEnabled == enabledNews && n.IsVerified == false)).ToListAsync();
            return _noticias;
        }

        public async Task<List<Noticia>> GetNoticiasByVeredicto(bool veredicto = true, bool imprecisa = true)
        {
            // Este metodo retorna uan lista de noticia segun su veredicto, por defecto retorna las noticias imprecisas 
            List<Noticia> noticias = null;

            if(veredicto && !imprecisa)
            {
                noticias = await _context.Noticia.Where(n => n.Veredicto == 2 && (n.IsEnabled == true && n.IsVerified == true)).ToListAsync();
            }
            else if(!veredicto && !imprecisa)
            {
                noticias = await _context.Noticia.Where(n => n.Veredicto == 1 && (n.IsEnabled == true && n.IsVerified == true)).ToListAsync();
            }
            else if(veredicto && imprecisa)
            {
                noticias = await _context.Noticia.Where(n => n.Veredicto == 3 && (n.IsEnabled == true && n.IsVerified == true)).ToListAsync();
            }

            return noticias;
        }
    }
}
