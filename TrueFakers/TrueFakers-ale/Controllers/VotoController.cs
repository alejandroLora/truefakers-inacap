﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrueFakers_ale.Data;
using TrueFakers_ale.Models;

namespace TrueFakers_ale.Controllers
{
    public class VotoController : Controller
    {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<Usuario> userManager;

        public VotoController(ApplicationDbContext context, UserManager<Usuario> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        public async Task<IActionResult> VotarVerdadera(int id)
        {
            bool flag = await CrearVoto(2, id);

            if(flag)
            {
                return RedirectToAction("Details", "Noticia", new { Id = id });
            }
            else
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> VotarFalsa(int id)
        {
            bool flag = await CrearVoto(1, id);

            if (flag)
            {
                return RedirectToAction("Details", "Noticia", new { Id = id });
            }
            else
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> VotarImprecisa(int id)
        {
            bool flag = await CrearVoto(3, id);

            if (flag)
            {
                return RedirectToAction("Details", "Noticia", new { Id = id });
            }
            else
            {
                return NotFound();
            }
        }

        public async Task<bool> CrearVoto(int veredicto, int idNoticia)
        {
            string currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            Usuario usuario = await GetUserById(currentUserId);
            Noticia noticia = await GetNoticia(idNoticia);
            Voto voto = _context.Voto.Where(v => v.Usuario == usuario && v.Noticia == noticia).FirstOrDefault();
            int result = 0;
            
            if(voto != null)
            {
                voto.Tipo = veredicto;
            }
            else
            {
                voto = new Voto();
                voto.Tipo = veredicto;
                voto.Noticia = noticia;
                voto.Usuario = usuario;
                _context.Voto.Add(voto);
            }

            result = await _context.SaveChangesAsync();

            if (result != 0)
                return true;
            else
                return false;
            
        }

        public async Task<Noticia> GetNoticia(int? id)
        {
            Noticia noticia = await _context.Noticia.Include(n => n.Usuario).Include(n => n.Comentarios).FirstOrDefaultAsync(m => m.NoticiaId == id);
            return noticia;
        }

        public async Task<Usuario> GetUserById(string currentUserId)
        {
            Usuario user = await userManager.FindByIdAsync(currentUserId);
            return user;
        }
    }
}
