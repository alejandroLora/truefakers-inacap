﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrueFakers_ale.Data;
using TrueFakers_ale.Models;

namespace TrueFakers_ale.Controllers
{
    public class ComentarioController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<Usuario> userManager;

        public ComentarioController(ApplicationDbContext context, UserManager<Usuario> userManager)
        {
            _context = context;
            this.userManager = userManager;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Comentar(string nuevoComentario, int noticiaId)
        {
            if(ModelState.IsValid)
            {
                ViewData["Comentario"] = nuevoComentario;
                string currentUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                Comentario comentario = new Comentario
                {
                    Contenido = nuevoComentario,
                    Fecha = DateTime.Now,
                    Estado = 0,
                    Puntaje = 0,
                    Reportes = 0,
                    Usuario = await GetUserById(currentUserId),
                    Noticia = await GetNoticia(noticiaId)
                };

                _context.Comentario.Add(comentario);

                int result = await _context.SaveChangesAsync();

                if (result != 0)
                {
                    return RedirectToAction("Details", "Noticia", new { Id = noticiaId});
                }
                else
                {
                    return NoContent();
                }
            }

            return View();
        }

        public async Task<IActionResult> Report(int id)
        {
            Comentario comentario = await _context.Comentario.Include(c => c.Noticia).FirstOrDefaultAsync(c => c.ComentarioId == id);
            comentario.Reportes++;
            int result = await _context.SaveChangesAsync();

            if(result != 0)
            {
                return RedirectToAction("Details", "Noticia", new { Id = comentario.Noticia.NoticiaId });
            }
            else
            {

            }

            return View();

        }

        public async Task<Usuario> GetUserById(string currentUserId)
        {
            Usuario user = await userManager.FindByIdAsync(currentUserId);
            return user;
        }

        public async Task<Noticia> GetNoticia(int? id)
        {
            Noticia noticia = await _context.Noticia.Include(n => n.Usuario).Include(n => n.Comentarios).FirstOrDefaultAsync(m => m.NoticiaId == id);
            return noticia;
        }
    }
}
