# Que es *TrueFakers*?


### Este proyecto se inició dentro de la asignatura Programación Avanzada en la universidad Inacap.
### Somos un grupo de 3 personas que tuvieron la idea de combatir las conocidas Fake News.
### El objetivo de *TrueFakers* es combatir las *Fake News* creando una plataforma Web que incentive a las personas que quieran informarse y ser parte de la verificacion de las noticias que rondan en el mundo del internet.  

## Estado del proyecto
**Este proyecto esta en DESARROLLO**


## Indice de Contenidos
 - Descripción de la Arquitectura utilizada
 - RoadMap
 - Diagramas
     - Diagrama de clases
     - Model de base de datos
     - Árbol de navegación de vistas
 - Instalación
     - Instrucciones
     - Requerimientos de sistema
     - Dependencias Frontend
     - Dependencias Backend
     - Versionamiento Git  
     
  
## Arquitectura

### Como arquitectura de proyecto elegimos Model View Controller. En esta arquitectura separamos la lógica de negocios de la estructura de vistas, otorgandole a cada sección de código su propia sección y organizarlas en torno a su proposito.

![mvc](https://i.ytimg.com/vi/xTPzz_2jbDc/maxresdefault.jpg)  
  


## Roadmap
![roadmap](img_readme/roadmap.jpeg)



## Diagramas

### Diagrama de Clases



![diagrama_clases](img_readme/clases.png)



### Base de Datos MER



![base_de_datos](img_readme/datos.jpeg)



### Árbol de navegación

![arbol_de_navegacion](img_readme/views.jpeg)



## Instalación



### Instrucciones de instalación

 - Descargar un IDE de C# de tu preferencia.
 - Instalar NET CORE 3.1 en tu sistema.
 - Clonar nuestro repositorio.
 - Cambiar la configuración para utlizar tu base de datos personal en archivo appsettings.json

 - ```json
    "ConnectionStrings": {
        "DefaultConnection": "Server=DESKTOP-UFAEK2L;Database=TrueFakerDB-Ale;
                             Trusted_Connection=True",
    },
    ````
 - Cambia el valor de "DefaultConnection" con la configuración de tu base de datos.
 - Prueba sus funcionalidades!

### Dependencias backend y frontend



![dependencias](img_readme/dependencies.jpeg)



### Versiones del C#



![C#](img_readme/net_version.jpeg)