﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TrueFakers_Web_Prototipo.Controllers
{
    public class NoticiaController : Controller
    {
        public IActionResult LeerNoticia()
        {
            return View();
        }
        public IActionResult ListaNoticias()
        {
            return View();
        }

        public IActionResult CrearNoticia()
        {
            return View();
        }
    }
}
