﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFakers_Web_Prototipo.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public string Contenido { get; set; }
        public DateTime Timestamp { get; set; }
        public int UsuarioId { get; set; }
        public int NoticiaId { get; set; }
        public Noticia Noticia { get; set; }
    }
}
