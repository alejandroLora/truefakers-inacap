﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFakers_Web_Prototipo.Models
{
    public class Multimedia
    {
        public int Id { get; set; }
        public string Filepath { get; set; }
        public int NoticiaId { get; set; }
    }
}
