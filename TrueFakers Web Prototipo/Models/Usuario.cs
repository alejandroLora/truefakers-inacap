﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFakers_Web_Prototipo.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Mail { get; set; }
        public string Clave { get; set; }
        public int Rol { get; set; }
        public ICollection<Noticia> Noticias { get; set; }
    }
}
