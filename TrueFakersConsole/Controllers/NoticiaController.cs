﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrueFakersConsole.Data;
using TrueFakersConsole.Models;
using TrueFakersConsole.View;

namespace TrueFakersConsole.Controllers
{
    public class NoticiaController
    {


        public void AgregarNoticia(Noticia noticia, int usuarioId)
        {
            noticia.Titulo = "Ingresa Titulo de tu noticia!".IngresoDeValorString();
            noticia.Cuerpo = "Ingresa el Curepo de tu noticia!".IngresoDeValorString();
            noticia.Fecha = DateTime.Now;
            noticia.UsuarioId = usuarioId;
            using(var ContextoDb = new ContextDB())
            {
                ContextoDb.Add(noticia);
                ContextoDb.SaveChanges();
            }
        }
        public static void LeerNoticia(int n)
        {
            using (var context = new ContextDB())
            {
                var noticia = context.Noticias.FirstOrDefault(x => x.Id == n);
                if (noticia != null)
                {
                    string veredicto = "(Noticia ";
                    switch (noticia.Veredicto)
                    {
                        case 0:
                            veredicto += "Falsa.)";
                            break;
                        case 1:
                            veredicto += "Verdadera.)";
                            break;
                        default:
                            veredicto += "No verificada.)";
                            break;
                    }

                    Console.WriteLine(noticia.Titulo + " - " + veredicto);
                    Console.WriteLine(noticia.Cuerpo);

                }
            }
        }

        public List<Noticia> ListarNoticias(int filtro)
        {
            List<Noticia> listaNoticias = new List<Noticia>();
            string veredicto = "";
            listaNoticias = BuscarNoticiasSegunFiltro(filtro);

            foreach (var item in listaNoticias)
            {
                if (listaNoticias != null || listaNoticias.Count == 0)
                {

                    if (item.Veredicto == 1)
                        veredicto = "Verdadera";
                    if (item.Veredicto == 2)
                        veredicto = "Falsa";
                    if (item.Veredicto == 0)
                        veredicto = "No verificada";

                    Console.WriteLine($"{ item.Id } \t { item.Titulo }  \t { item.Fecha.Date }  \t { veredicto }");


                }
                else
                {
                    Console.WriteLine("No se pudo determinar el filtro");
                }
            }

            Console.ReadLine();
            return listaNoticias;
                Console.WriteLine();
        }


        public List<Noticia> BuscarNoticiasSegunFiltro(int filtro)
        {
            List<Noticia> listaNoticias = new List<Noticia>();

            using (var ContextoDb = new ContextDB())
            {
                
                if (filtro == 1) //Lista noticias verificadas
                {
                    listaNoticias = ContextoDb.Noticias.Where(n => n.Estado != 0 && n.Veredicto == 1  || n.Veredicto == 2 ).ToList(); 
                }
                else if (filtro == 2) //Lista noticias no verificadas
                {
                    listaNoticias = ContextoDb.Noticias.Where(n => n.Estado != 0 && n.Veredicto == 0).ToList(); 
                }
                else if (filtro == 3) //Lista noticias Verdaderas
                {
                    listaNoticias = ContextoDb.Noticias.Where(n => n.Estado != 0 && n.Veredicto == 1).ToList(); 
                }
                else if (filtro == 4) //Lista noticias Falsas
                {
                    listaNoticias = ContextoDb.Noticias.Where(n => n.Estado != 0 && n.Veredicto == 2).ToList();
                }
                else if (filtro == 5) //Lista de todas las noticias habilitadas
                {
                    listaNoticias = ContextoDb.Noticias.Where(n => n.Estado != 0).ToList();
                }
                else if (filtro == 6) //Lista de todas las noticias (incluye deshabilitadas)
                {
                    listaNoticias = ContextoDb.Noticias.ToList();
                }
                else
                {
                    listaNoticias = null;
                }
            }

            return listaNoticias;

        }

        public void DeshabilitarNoticia(int idNoticia)
        {
            using (var context = new ContextDB())
            {
                Console.Clear();
                var noticia = context.Noticias.FirstOrDefault(item => item.Id == idNoticia);
                if(noticia != null)
                {
                    noticia.Estado = 0;
                    context.SaveChanges();
                    Console.WriteLine("Noticia deshabilitada.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("No se ha encontrado la noticia.");
                    Console.ReadLine();
                }

            }

        }

        public void AprobarNoticia(int idNoticia)
        {
            using (var context = new ContextDB())
            {
                Console.Clear();
                var noticia = context.Noticias.FirstOrDefault(item => item.Id == idNoticia);
                noticia.Estado = 1;
                context.SaveChanges();
                Console.WriteLine("Noticia aprobada.");
                Console.ReadLine();

            }
        }

        public void EditarVeredicto(int idNoticia)
        {
            using (var context = new ContextDB())
            {
                Console.Clear();
                var noticia = context.Noticias.FirstOrDefault(item => item.Id == idNoticia);
                if (noticia != null)
                {
                    string veredicto = "";
                    if (noticia.Veredicto == 1)
                        veredicto = "Verdadera";
                    if (noticia.Veredicto == 2)
                        veredicto = "Falsa";
                    if (noticia.Veredicto == 0)
                        veredicto = "No verificada";
                    Console.WriteLine();
                    Console.WriteLine("********Noticia a editar**********");
                    Console.WriteLine();
                    Console.WriteLine($"{ noticia.Id } \t { noticia.Titulo }  \t { noticia.Fecha.Date }  \t { veredicto }");

                }
                else
                {
                    Console.WriteLine("No se pudo determinar el filtro");
                    return;
                }

                Console.WriteLine("*************************");
                Console.WriteLine("Seleccionar nuevo veredicto.");
                Console.WriteLine();
                Console.WriteLine("1. No verificada.");
                Console.WriteLine("2. Verdadera.");
                Console.WriteLine("3. Falsa.");
                Console.WriteLine();
                Console.WriteLine("*************************");
                var opcion = int.Parse(Console.ReadLine())-1;
                noticia.Veredicto = opcion;

                //noticia.Estado = 0;
                context.SaveChanges();
                Console.WriteLine("Veredicto modificado.");
                Console.ReadLine();
            }
        }
    }
}
