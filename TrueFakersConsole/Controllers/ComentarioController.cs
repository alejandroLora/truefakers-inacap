﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrueFakersConsole.Data;
using TrueFakersConsole.Models;
using TrueFakersConsole.View;

namespace TrueFakersConsole.Controllers
{
    public class ComentarioController
    {
        public void AgregarComentario(Comentario comentario, int noticiaId, int usuarioId)
        {
            Console.Clear();
            ListarComentariosPorNoticia(noticiaId);

            comentario.Contenido = "Ingresa tu comentario!".IngresoDeValorString();
            comentario.Timestamp = DateTime.Now;
            comentario.NoticiaId = noticiaId;
            comentario.UsuarioId = usuarioId;

            using(var ContextoDb = new ContextDB())
            {
                ContextoDb.Add(comentario);
                ContextoDb.SaveChanges();
            }
        }

        public void ListarComentariosPorNoticia(int noticiaId/*, int usuarioId*/)
        {
            using (var ContextoDb = new ContextDB())
            {
                List<Comentario> listaComentarios = ContextoDb.Comentarios.Where(c => c.NoticiaId == noticiaId).ToList();

                foreach (var item in listaComentarios)
                {
                    Console.WriteLine($" {BuscarUsuarioSegunId(item.UsuarioId)}: {item.Contenido}");
                }
            }
        }

        public string BuscarUsuarioSegunId(int usuarioId)
        {
            string mailUsuario = "";
            using (var ContextoDb = new ContextDB())
            {
                Usuario usuario = ContextoDb.Usuarios.Where(u => u.Id == usuarioId).SingleOrDefault();

                if (usuario != null)
                {
                    mailUsuario = usuario.Mail;
                }
            }

            return mailUsuario;
        }
    }
}
