﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrueFakersConsole.Data;
using TrueFakersConsole.Models;
using TrueFakersConsole.View;

namespace TrueFakersConsole.Controllers
{
    public class UsuarioController
    {
        ConsoleMsg consoleMsg = new ConsoleMsg();

        public Usuario Login()
        {
            Console.Clear();

            string Mail = "Ingresa tu mail".IngresoDeValorString();
            string Clave = "Ingresa tu contraseña".IngresoDeValorString();
            Usuario UsuarioVerificado = new Usuario();

            using (var ContextoDb = new ContextDB())
            {

                UsuarioVerificado = ContextoDb.Usuarios.Where(u => u.Mail == Mail && u.Clave == Clave).SingleOrDefault();

                if (UsuarioVerificado == null)
                {
                    Console.WriteLine("Contraseña o Mail incorrectos");
                    Console.ReadLine();
                    Login();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Ingreso exitoso!");
                    consoleMsg.MenuSegunTipoUsuario(UsuarioVerificado);
                    Console.ReadLine();
                }

            }

            return UsuarioVerificado;
        }

        public void RegistrarController(Usuario usuario)
        {
            Console.Clear();
            usuario.Mail = "Ingresa tu mail".IngresoDeValorString();
            usuario.Clave = "Ingresa tu clave".IngresoDeValorString();
            usuario.Rol = "Ingresa el rol que quieres realizar [ 0: Regular // 1: Editor // 2: Administrador ]".IngresoDeValorInt(0, 2);

            using (var ContextoDb = new ContextDB())
            {

                if (!UsuarioExistentePorMail(usuario.Mail))
                {
                    ContextoDb.Add(usuario);
                    ContextoDb.SaveChanges();
                    Console.WriteLine("Registro exitoso!");
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Usuario ya existe!");
                    Console.WriteLine("Deseas intentar de nuevo? [0: NO  //  1: SI ]");
                    int IntentarDenuevo = int.Parse(Console.ReadLine());

                    if (IntentarDenuevo == 1)
                    {
                        usuario = new Usuario();
                        RegistrarController(usuario);
                    }
                }

            }
        }

        private bool UsuarioExistentePorMail(string mail)
        {
            bool existe = false;

            using (var ContextoDb = new ContextDB())
            {

                Usuario VerificacionUsuario = new Usuario();
                VerificacionUsuario = ContextoDb.Usuarios.Where(u => u.Mail == mail).SingleOrDefault();

                if (VerificacionUsuario != null)
                {
                    existe = true;
                }
            }

            return existe;
        }
    }
}
