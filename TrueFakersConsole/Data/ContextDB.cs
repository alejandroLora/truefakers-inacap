﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TrueFakersConsole.Models;

namespace TrueFakersConsole.Data
{
    class ContextDB : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Noticia> Noticias { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Multimedia> Multimedias { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=MATPC\SQLEXPRESS2;Initial Catalog=TrueFakersSimple1;Persist Security Info=True;User ID=sa;Password=123");
        }
    }
}
