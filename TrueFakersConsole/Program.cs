﻿using System;
using TrueFakersConsole.Controllers;
using TrueFakersConsole.Models;
using TrueFakersConsole.View;

namespace TrueFakersConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            UsuarioController usuarioController = new UsuarioController();

            int Opcion = 0;
            while (Opcion != 3)
            {
                ConsoleMsg.PrimeraVentanaOpciones();
                Opcion = int.Parse(Console.ReadLine());

                switch (Opcion)
                {
                    case 1:
                        usuarioController.Login();
                        
                        break;

                    case 2:
                        Usuario usuario = new Usuario();
                        usuarioController.RegistrarController(usuario);
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
