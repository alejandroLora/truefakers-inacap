﻿using System;
using System.Collections.Generic;
using System.Text;
using TrueFakersConsole.Controllers;
using TrueFakersConsole.Models;

namespace TrueFakersConsole.View
{
    public class ConsoleMsg
    {
        NoticiaController noticiaController = new NoticiaController();
        ComentarioController comentarioController = new ComentarioController();

        public static void PrimeraVentanaOpciones()
        {
            Console.WriteLine("*************************");
            Console.WriteLine();
            Console.WriteLine("1. Inicia sesion");
            Console.WriteLine("2. Registrate");
            Console.WriteLine();
            Console.WriteLine("*************************");
        }

        public void MenuSegunTipoUsuario(Usuario usuario)
        {
            switch (usuario.Rol)
            {
                case 0:
                    OpcionesUsuarioRegistrado(usuario);
                    break;

                case 1:
                    OpcionesUsuarioEditor();
                    break;

                case 2:
                    break;

                default:
                    break;
            }
        }

        private void OpcionesUsuarioRegistrado(Usuario usuario)
        {
            int Opcion = 0;
            while (Opcion != 4)
            {
                ListaDeNoticias();
                Console.WriteLine("*************************");
                Console.WriteLine();
                Console.WriteLine("1. Publicar una noticia.");
                Console.WriteLine("2. Leer noticia.");
                Console.WriteLine("3. Seleccione una noticia para comentar.");
                Console.WriteLine("4. Buscar noticia segun filtros.");
                Console.WriteLine("5. Cerrar Sesion.");
                Console.WriteLine();
                Console.WriteLine("*************************");
                Console.WriteLine();

                int.TryParse(Console.ReadLine(),out Opcion);
                int noticiaId;
                //var nController = new NoticiaController();
                switch (Opcion)
                {
                    case 1:
                        Noticia noticia = new Noticia();
                        noticiaController.AgregarNoticia(noticia, usuario.Id);
                        break;

                    case 2:
                        ListaDeNoticias();
                        Console.WriteLine("Ingresar id de noticia que desea leer.");
                        int.TryParse(Console.ReadLine(), out noticiaId);
                        //Comentario comentario = new Comentario();
                        NoticiaController.LeerNoticia(noticiaId);
                        Console.ReadLine();
                        break;
                    
                    case 3:
                        ListaDeNoticias();
                        noticiaId = LeerNoticiaId();
                        Comentario comentario = new Comentario();
                        comentarioController.AgregarComentario(comentario, noticiaId, usuario.Id);
                        break;

                    case 4:
                        OpcionesFiltrarNoticia();
                        break;
                    case 5:
                        break;
                    default:
                        break;
                }
            }
        }

        private void OpcionesUsuarioEditor()
        {
            var opcion = 0;
            while(opcion != 4)
            {
                ListaDeNoticias();
                Console.WriteLine("*************************");
                Console.WriteLine();
                Console.WriteLine("1. Deshabilitar Noticia.");
                Console.WriteLine("2. Aprobar una noticia");
                Console.WriteLine("3. Editar veredicto");
                Console.WriteLine("4. Cerrar sesion");
                Console.WriteLine();
                Console.WriteLine("*************************");

                opcion = int.Parse(Console.ReadLine());
                int idNoticia;
                var nController = new NoticiaController();
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Seleccionar id de noticia a deshabilitar.");
                        idNoticia = int.Parse(Console.ReadLine());
                        nController.DeshabilitarNoticia(idNoticia);

                        break;
                    case 2:
                        Console.WriteLine("Seleccionar id de noticia por aprobar.");
                        idNoticia = int.Parse(Console.ReadLine());
                        nController.AprobarNoticia(idNoticia);

                        break;
                    case 3:
                        Console.WriteLine("Seleccionar id de noticia a modificar.");
                        idNoticia = int.Parse(Console.ReadLine());
                        nController.EditarVeredicto(idNoticia);

                        break;
                    default:
                        break;
                }
            }
            


        }

        private void ListaDeNoticias()
        {
            Console.Clear();
            Console.WriteLine("Todas nuestras noticias:");
            Console.WriteLine();
            noticiaController.ListarNoticias(5);
            Console.WriteLine();
        }

        private int LeerNoticiaId()
        {
            string noticiaIdstring = "Selecciona noticia que deseas comentar".IngresoDeValorString();
            int NoticiaId = int.Parse(noticiaIdstring);
            return NoticiaId;
        }

        private void OpcionesFiltrarNoticia()
        {
            Console.Clear();
            Console.WriteLine("Ingresa un tipo de filtro: ");
            Console.WriteLine("1: Noticias Verificadas");
            Console.WriteLine("2. Noticias no Verificadas");
            Console.WriteLine("3. Noticias Verdaderas");
            Console.WriteLine("4. Noticias Falsas");
            int SeleccionDeUsuario = "Ignrese el filtro que desee utilizar".IngresoDeValorInt(1, 4);
            noticiaController.ListarNoticias(SeleccionDeUsuario);
        }
    }
}
