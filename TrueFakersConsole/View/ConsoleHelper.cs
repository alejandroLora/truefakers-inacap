﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFakersConsole.View
{
    public static class ConsoleHelper
    {
        public static string IngresoDeValorString(this string message)
        {
            string output = "";

            while (string.IsNullOrWhiteSpace(output))
            {
                Console.WriteLine(message);
                output = Console.ReadLine();
            }

            return output;
        }

        public static int IngresoDeValorInt(this string message, int valorMaximo, int valorMInimo)
        {
            int output = 0;
            bool ValidParse = false;
            bool ValidRange = false;

            while (ValidParse == false || ValidRange == true)
            {
                Console.WriteLine(message);
                ValidParse = int.TryParse(Console.ReadLine(), out output);

                ValidRange = (output >= valorMInimo && output <= valorMaximo);
            }

            return output;
        }

    }
}
