﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrueFakersConsole.Models
{
    public class Noticia
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public int Estado { get; set; } = 1;
        public string Titulo { get; set; }
        public string Cuerpo { get; set; }
        public int Veredicto { get; set; } = 0;
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
        public ICollection<Comentario> Comentarios { get; set; }
        public ICollection<Multimedia> Multimedias { get; set; }
    }
}
