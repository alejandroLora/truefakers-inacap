﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrueFakersConsole.Models
{
    public class Multimedia
    {
        public int Id { get; set; }
        public string Filepath { get; set; }
        public int NoticiaId { get; set; }
    }
}
